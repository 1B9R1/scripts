/**
 * 把音频地址抽离出来
 */
const fs = require('fs')
const book = 'Romeo_and_Juliet'
const chapters = fs.readdirSync(`./books/${book}`)
let str = ''

chapters.forEach(text => {
  const content = fs.readFileSync(`./books/${book}/${text}`, 'utf-8')
  const data = JSON.parse(content)
  str += data.article_info.audio_info.audio_url + '\n'
  // console.log(data.article_info.audio_info.audio_url)
})

fs.writeFileSync(`./audio/${book}.txt`, str)
console.log('complete')