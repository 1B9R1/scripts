const https = require('https')
const querystring = require('querystring')
const fs = require('fs')

const fetch = (path, callback, method = 'GET', params = {}) => {
  const req = https.request({
    hostname: 'reading.baicizhan.com',
    path,
    method,
    headers: {
      'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36 MicroMessenger/6.5.2.501 NetType/WIFI WindowsWechat QBCore/3.43.901.400 QQBrowser/9.0.2524.400',
      Cookie: 'readin_token=BAhJIiFvXzNIRnZ5T1p5R1BWbVd3Tk1mYlZvR3dueXd3BjoGRVQ%3D--542699964a12592a7f0cc043924a419137d0584e; _wechat_session=BAh7B0kiD3Nlc3Npb25faWQGOgZFVEkiJTdjNjJhYjljNmVjNTQ3NzY2NTJjNThlYWE2ZDBiZGU3BjsAVEkiEF9jc3JmX3Rva2VuBjsARkkiMUo2T2thMDQ2TFZ6Q2pKQkdWK1FmbmdKOG4yeVVBMXVrMHN3RHV3Z3R5aVk9BjsARg%3D%3D--b1b6727623badb6944c0c12f59fba8b4829711b4; access_token=gZq%2B6Nls416bH8JC7xU83ZWpo14Vmh%2Fxx7F4rkmS0xU%3D',
    }
  }, (res) => {
    let data = ''

    res.on('data', (d) => {
      data += d
    })

    res.on('end', () => {
      callback(data)
    })
  })

  req.on('error', (e) => {
    console.error(`request ${path} has a problem: ${e.message}`)
  });

  req.write(querystring.stringify(params))
  req.end()
}

const dates = []

for (let i = 1; i < 21; i++) {
  const day = i < 10 ? '0' + i : i
  dates.push(`2018-09-${day}`)
}

const book = 'Romeo_and_Juliet'
const authenticity_token = 'J6Oka046LVzCjJBGV%2BQfngJ8n2yUA1uk0swDuwgtyiY%3D'

dates.forEach(date => {
  fetch(`/api/1536026311000/get_reading_data.json?date=${date}&authenticity_token=${authenticity_token}`, (data) => {
    // console.log(JSON.parse(data))
    fs.writeFileSync(`./books/${book}/${date}.txt`, data)
  })
})