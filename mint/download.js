const fs = require('fs')
const http = require('http')
const book = 'Romeo_and_Juliet'
const chapters = fs.readdirSync(`./books/${book}`)

const download = (idx, path) => {
  const req = http.request({
    hostname: 'tx.bczcdn.com',
    path,
    method: 'GET',
  }, (res) => {
    const buffers = []

    res.on('data', (chunk) => { // chunk 是 Buffer 类型
      buffers.push(chunk)
    })

    res.on('end', () => {
      fs.writeFileSync(`./audios/${book}/${idx}.mp3`, Buffer.concat(buffers))
      console.log(`《${book}》第${idx}部分音频下载完成`)
    })
  })

  req.on('error', (e) => {
    console.error(`request ${path} has a problem: ${e.message}`)
  });

  req.end()
}
console.log(`共${chapters.length}部分：`)
chapters.forEach((text, idx) => {
  const content = fs.readFileSync(`./books/${book}/${text}`, 'utf-8')
  const data = JSON.parse(content)
  const url = data.article_info.audio_info.audio_url.replace('//tx.bczcdn.com', '')

  download((idx + 1), url)
})