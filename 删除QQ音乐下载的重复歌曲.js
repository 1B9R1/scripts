const fs = require('fs')

let deleteCount = 0;
let dirPath = 'D:/Music/QQ';
let files = fs.readdirSync(dirPath)

console.log(`QQ音乐Download目录：${dirPath}`);

files.forEach((fileName) => {
  if (/\(\d+\)\./.test(fileName)) {
    console.log('删除：' + fileName)
    deleteCount += 1
    fs.unlinkSync('D:/Music/QQ/' + fileName);
  }
})

console.log(`共删除了${deleteCount}个文件`)
