// 导入库文件
const fs = require('fs')
const http = require('http')
const https = require('https')
const querystring = require('querystring')

// 定义变量
let authToken = ''
let books = [
  'Phonics 1 26个字母的拼读规则',
  'Phonics 2 元音字母在重读音节中的读音',
  'Phonics 3 元音字母在重读音节中的特殊读音',
  'Phonics 4 元音字母在非重读音节中的读音',
  'Phonics 5 元音字母组合的读音规则',
  'Phonics 6 辅音字母组合的读音规则',
  'Phonics 7 元音的字母组合规则',
  'Phonics 8 特殊音节的读音规则',
  'Phonics: 自然拼读',
]
let lessons = []
let lessonIndex = 0

// 定义方法
/**
 * 序列化GET的请求参数
 * @param {object} obj 
 */
const normalize = (obj) => {
  let str = ''

  for (let key in obj) {
    let value = obj[key] + ''
    if (value) {
      value = value.replace(/=/g, '%3D').replace(/\+/g, '%2B').replace(/\//g, '%2F')
    }
    str += '&' + key + '=' + value
  }

  // console.log(str)
  return str.substring(1)
}

/**
 * 生成passport的url请求
 * @param {string} url 
 * @param {object} param 
 */
const getRequestOptions = (obj, data) => {
  return Object.assign({}, {
    host: 'ah.kouyu100.com',
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Content-Length': Buffer.byteLength(data),
      'Cookie': `authToken=${authToken}`
    }
  }, obj)
}

const createRequest = (options, data, callback) => {
  const postData = querystring.stringify(data)
  const req = http.request(getRequestOptions(options, postData), (res) => {
    res.setEncoding('utf8')

    let data = ''
    // console.log(`STATUS: ${res.statusCode}`)
    // console.log(`HEADERS: ${JSON.stringify(res.headers)}`)

    res.on('data', (chunk) => {
      // console.log(`BODY: ${chunk}`)
      data += chunk
    })

    res.on('end', () => {
      // console.log('No more data in response.')
      // console.log(data)
      callback(data)
    })
  })

  req.on('error', (e) => {
    console.error(`problem with request: ${e.message}`)
  })

  return {
    send() {
      req.write(postData)
      req.end()
    }
  }
}

// bookId: 922, 1133 - 1140
// http://ah.kouyu100.com/lovesunshine/showLesson.action?book_id=1133
const fetchLessonName = (bookId) => {
  createRequest({
    method: 'GET',
    path: `/lovesunshine/showLesson.action?book_id=${bookId}`
  }, null, (data) => {
    console.log(`Fetch book #${bookId} name complete.`)
    const dirPath = `./books/${books[bookId - 1133]}`
    let list

    try {
      fs.mkdirSync(dirPath)
    } catch (error) {
      
    }

    try {
      list = data.match(/title="[^"]+"/g).map((l) => {
        return l.match(/title="([^"]+)"/)[1]
      })
    } catch (error) {
      console.log(list)
      return
    }
    
    list.forEach((l) => {
      const path = `${dirPath}/${l}.txt`
      lessons.push(path)
      fs.writeFileSync(path)
    })

    bookId += 1

    setTimeout(() => {
      if (bookId < 1141) {
        fetchLessonName(bookId)
      } else {
        console.log(`共有${lessons.length}个课程`)
        fetchPhonics(45572)
      }
    }, 500)
  }).send()
}

const fetchPhonics = (lessonId) => {
  createRequest({
    path: '/lovesunshine/initWordChoose.action'
  }, {
    lessonId
  }, (data) => {
    console.log(`Fetch phonics #${lessonId} complete.`)
    const file = data.replace(/@@/g, '\n')
    fs.writeFileSync(lessons[lessonIndex], file)

    lessonIndex += 1
    lessonId += 1

    if (lessonId === 45611) {
      lessonId = 45644
    } else if (lessonId === 45669) {
      lessonId = 45694
    } else if (lessonId === 45703) {
      lessonId = 45752
    }

    if (lessonId < 45774) { // 45774
      fetchPhonics(lessonId)
    }
  }).send()
}

// 主程序入口
createRequest({
  path: '/lovesunshine/getTokenFromServer.action'
}, {
  domain: 'lovesunshine',
  userName: 'lst003',
  userPwd: '999888'
}, (data) => {
  console.log('Login success...')
  authToken = data.split('&')[0]
  // fetchPhonics(45572)
  fetchLessonName(1133)
}).send()