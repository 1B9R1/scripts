// 导入库文件
const fs = require('fs')
const http = require('http')
const https = require('https')
const querystring = require('querystring')

// 定义变量
let authToken = ''
let group = [2296, 2055, 2165, 2163, 2162, 2161]
let groupName = {
  2296: '初三',
  2055: '初一',
  2165: '初二',
  2163: '初二',
  2162: '初三',
  2161: '初三'
}
let exams = []
let options = 'ABCDEFG'

// 定义方法
/**
 * 生成passport的url请求
 * @param {string} url 
 * @param {object} param 
 */
const getRequestOptions = (obj, data) => {
  return Object.assign({}, {
    host: 'ah.kouyu100.com',
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Content-Length': Buffer.byteLength(data),
      'Cookie': `authToken=${authToken}`
    }
  }, obj)
}

const createRequest = (options, data, callback) => {
  const postData = querystring.stringify(data)
  const req = http.request(getRequestOptions(options, postData), (res) => {
    let data = ''

    res.setEncoding('utf8')
    res.on('data', (chunk) => {
      data += chunk
    })
    res.on('end', () => {
      callback(data)
    })
  })

  req.on('error', (e) => {
    console.error(`problem with request: ${e.message}`)
  })

  return {
    send() {
      req.write(postData)
      req.end()
    }
  }
}

const fetchExamName = (bookId) => {
  // console.log(`/lovesunshine/findExamList4Ceshi.action?bookId=${bookId}&examType=1`)
  createRequest({
    method: 'GET',
    path: `/lovesunshine/findExamList4Ceshi.action?bookId=${bookId}&examType=1`
  }, null, (data) => {
    console.log(`Fetch exam #${bookId} name complete.`)
    const list = JSON.parse(data).examList
    // console.log(list)

    list.forEach((exam) => {
      const examId = exam.id
      exams.push(bookId + '-' + examId)
      // fs.writeFileSync(`./exams/${examId}.txt`)
    })

    setTimeout(() => {
      if (group.length > 0) {
        fetchExamName(group.shift())
      } else {
        console.log(`Fetch [${exams.length}] exam name complete, now start fetch exam...`)
        fetchExam(exams.shift())
      }
    }, 100)
  }).send()
}

const fetchExam = (examTag) => {
  const groupId = examTag.split('-')[0]
  const examId = examTag.split('-')[1]
  createRequest({
    method: 'GET',
    path: `/lovesunshine/getListenGroupsByExamId.action?examId=${examId}`
  }, null, (data) => {
    console.log(`Fetch exam #${examId} complete.`)
    let list
    let text = ''

    try {
      list = JSON.parse(data).groupList
    } catch (error) {
      console.log(data)
      return
    }

    text += `试卷音频：http://exam.kouyu100.com${list[0].mediaFileUrl}\n\n`
    // 第一大题
    text += '第一大题\n'
    list[0].chooseList.forEach((question, index) => {
      text += `A. http://exam.kouyu100.com${question.item1}\n`
      text += `B. http://exam.kouyu100.com${question.item2}\n`
      text += `C. http://exam.kouyu100.com${question.item3}\n`
      text += `答案：${question.answers}\n\n`
    })

    // 第二大题
    const loop = [1, 2, 3, 4]
    text += '第二大题\n'
    loop.forEach((n) => {
      list[n].chooseList.forEach((question, index) => {
        text += `问题：${question.title}\n`
        text += `A. ${question.item1}\n`
        text += `B. ${question.item2}\n`
        text += `C. ${question.item3}\n`
        text += `答案：${question.answers}\n\n`
      })
    })

    // 第三大题
    text += '第三大题\n'
    text += `图片：http://exam.kouyu100.com${list[6].extend}\n\n`
    list[5].chooseList.forEach((question, index) => {
      text += `问题：${question.title}\n`
      text += `答案1. ${question.item1}\n`
      text += `答案2. ${question.item2}\n`
      text += `答案3. ${question.item3}\n\n`
    })
    list[6].blankList.forEach((blank, index) => {
      text += `填空${index + 1}：${blank.answers}\n`
    })

    fs.writeFileSync(`./exams/${groupName[groupId]}-${examId}.txt`, text)

    setTimeout(() => {
      if (exams.length > 0) {
        fetchExam(exams.shift())
      } else {
        console.log(`Fetch exam complete!`)
      }
    }, 100)
  }).send()
}

// 主程序入口
createRequest({
  path: '/lovesunshine/getTokenFromServer.action'
}, {
  domain: 'lovesunshine',
  userName: 'lst003',
  userPwd: '999888'
}, (data) => {
  console.log('Login success...')
  authToken = data.split('&')[0]

  fetchExamName(group.shift())
}).send()