// 导入库文件
const fs = require('fs')
const http = require('http')
const https = require('https')
const querystring = require('querystring')

// 定义变量
let authToken = ''
let catalogs = []
let videos = []
let text = ''

// 定义方法
/**
 * 生成passport的url请求
 * @param {string} url 
 * @param {object} param 
 */
const getRequestOptions = (obj, data) => {
  return Object.assign({}, {
    host: 'ah.kouyu100.com',
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Content-Length': Buffer.byteLength(data),
      'Cookie': `authToken=${authToken}`
    }
  }, obj)
}

const createRequest = (options, data, callback) => {
  const postData = querystring.stringify(data)
  const req = http.request(getRequestOptions(options, postData), (res) => {
    let data = ''

    res.setEncoding('utf8')
    res.on('data', (chunk) => {
      data += chunk
    })
    res.on('end', () => {
      callback(data)
    })
  })

  req.on('error', (e) => {
    console.error(`problem with request: ${e.message}`)
  })

  return {
    send() {
      req.write(postData)
      req.end()
    }
  }
}

const fetchCatalog = (catalog) => {
  createRequest({
    method: 'GET',
    path: `/lovesunshine/findExamList4Ceshi.action?bookId=${catalog.id}&examType=3`
  }, null, (data) => {
    console.log(`Fetch [catalog] #${catalog.id} complete.`)
    videos = JSON.parse(data).examList
    fetchVideo(videos.shift(), catalog.name)
  }).send()
}

const fetchVideo = (video, catalogName) => {
  createRequest({
    method: 'GET',
    path: `/lovesunshine/getListenGroupsByExamId.action?examId=${video.id}`
  }, null, (data) => {
    console.log(`Fetch video #${video.id} complete.`)
    let name

    try {
      name = JSON.parse(data).groupList[0].title.match(/src='([^']+)'/)[1]
    } catch (error) {
      name = ''
    }

    text += `${catalogName}-${video.title}-${name}\n`

    setTimeout(() => {
      if (videos.length > 0) {
        fetchVideo(videos.shift(), catalogName)
      } else {
        text += '\n'

        if (catalogs.length > 0) {
          fetchCatalog(catalogs.shift())
        } else {
          fs.writeFileSync('./videos.txt', text)
          console.log('Fetch finish!')
        }
      }
    }, 300)
  }).send()
}

// 主程序入口
createRequest({
  path: '/lovesunshine/getTokenFromServer.action'
}, {
  domain: 'lovesunshine',
  userName: 'lst003',
  userPwd: '999888'
}, (data) => {
  console.log('Login success...')
  authToken = data.split('&')[0]

  createRequest({
    method: 'GET',
    path: '/lovesunshine/findListenBook4Ceshi.action?examType=3'
  }, null, (data) => {
    catalogs = JSON.parse(data).bookList
    console.log(catalogs.length)
    catalogs.shift()
    fetchCatalog(catalogs.shift())
  }).send()
}).send()