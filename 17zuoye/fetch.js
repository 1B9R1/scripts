const https = require('https')
const querystring = require('querystring')
const fs = require('fs')

const fetch = (path, callback, method = 'GET', params = {}) => {
  const req = https.request({
    hostname: 'www.17zuoye.com',
    path,
    method,
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      Cookie: 'lupld=1; voxauth=jgoJosfvo+xoj2OSosvrJJK+3OZi734yuPMHVlJzmTiYrxLp3NHdVabLEolUf+aHY2c0DPFARsh4c/iAeEpFLw; va_sess=jgoJosfvo+xoj2OSosvrJJK+3OZi734yuPMHVlJzmTiYrxLp3NHdVabLEolUf+aHY2c0DPFARsh4c/iAeEpFLw; uid=1869296',
    }
  }, (res) => {
    let data = ''

    res.on('data', (d) => {
      data += d
    })

    res.on('end', () => {
      callback(data)
    })
  })

  req.on('error', (e) => {
    console.error(`request ${path} has a problem: ${e.message}`)
  });

  // console.log(querystring.stringify(params))
  req.write(querystring.stringify(params))
  req.end()
}

const bookQueryList = [{
    level: 3,
    term: 1
  },
  {
    level: 3,
    term: 2
  },
  {
    level: 4,
    term: 1
  },
  {
    level: 4,
    term: 2
  },
  {
    level: 5,
    term: 1
  },
  {
    level: 5,
    term: 2
  },
  {
    level: 6,
    term: 1
  },
  {
    level: 6,
    term: 2
  },
]
let bookList = [
  // {
  //   id: 'BK_10300000293123',
  //   seriesId: 'BKC_10300010769960',
  //   name: '人教版-精通(三起)-三年级上'
  // },
  // {
  //   id: 'BK_10300000294371',
  //   seriesId: 'BKC_10300010769960',
  //   name: '人教版-精通(三起)-三年级下'
  // },
  // {
  //   id: 'BK_10300000295620',
  //   seriesId: 'BKC_10300010769960',
  //   name: '人教版-精通(三起)-四年级上'
  // },
  // {
  //   id: 'BK_10300000296408',
  //   seriesId: 'BKC_10300010769960',
  //   name: '人教版-精通(三起)-四年级下'
  // },
  // {
  //   id: 'BK_10300000299380',
  //   seriesId: 'BKC_10300010769960',
  //   name: '人教版-精通(三起)-五年级上'
  // },
  // {
  //   id: 'BK_10300000297597',
  //   seriesId: 'BKC_10300010769960',
  //   name: '人教版-精通(三起)-五年级下'
  // },
  {
    id: 'BK_10300000300193',
    seriesId: 'BKC_10300010769960',
    name: '人教版-精通(三起)-六年级上'
  },
  {
    id: 'BK_10300000298491',
    seriesId: 'BKC_10300010769960',
    name: '人教版-精通(三起)-六年级下'
  }
]
let unitList = []
let bookName = ''
let unitName = ''

const fetchBook = () => {
  const {
    level,
    term
  } = bookQueryList.shift()
  const query = querystring.stringify({
    level,
    term,
    subject: 'ENGLISH',
    _: Date.now()
  })

  fetch(`/teacher/new/homework/sortbook.api?${query}`, (res) => {
    res = JSON.parse(res)
    let books = res.rows.filter(book => {
      return /^人教版-精通\(三起\)/.test(book.name)
    })
    books = books.map(book => {
      return {
        id: book.id,
        seriesId: book.seriesId,
        name: book.name
      }
    })
    // console.log(books)
    bookList = bookList.concat(books)

    if (bookQueryList.length > 0) {
      fetchBook()
    } else {
      // console.log(bookList)
      fetchKnowledgPpoint()
    }
  })
}

const fetchUnit = () => { // 根据书本获取单元
  if (bookList.length > 0) {
    const {
      id,
      name
    } = bookList.shift()

    bookName = name

    fetch(`/teacher/new/homework/changebook.api`, (res) => { // 后台切换书本
      // console.log(res)
      res = JSON.parse(res)

      if (res.success) {
        const query = querystring.stringify({
          subject: 'ENGLISH',
          clazzs: '32090913_1298858',
          objectiveConfigId: 'OCN_02479037517',
          _: Date.now(),
        })

        fetch(`/teacher/new/homework/clazz/book.api?${query}`, (res2) => { // 获取单元
          res2 = JSON.parse(res2)

          if (res2.success) {
            // console.log(res2.clazzBook.unitList)
            unitList = res2.clazzBook.unitList
            fetchKnowledgPpoint(id)
          } else {
            console.log('Fetch unit list fail!')
          }
        })
      } else {
        console.log('Change book fail!')
      }
    }, 'POST', {
      clazzs: '32090913_1298858',
      bookId: id,
      subject: 'ENGLISH'
    })
  } else {
    console.log('抓取完成')
  }
}

const fetchKnowledgPpoint = (bookId) => { // 根据单元获取知识点
  if (unitList.length > 0) {
    const {
      unitId,
      cname
    } = unitList.shift()
    unitName = cname
    const filePath = `./assets/${bookName}-${unitName}.txt`

    try {
      fs.statSync(filePath)
      console.log(`${filePath} has fetched.`)
      fetchKnowledgPpoint(bookId)
    } catch (error) {
      const params = {
        bookId,
        unitId,
        sections: '',
        subject: 'ENGLISH',
        _: Date.now(),
      }

      let query = querystring.stringify(params)

      fetch(`/teacher/new/homework/objective/list.api?${query}`, res => {
        res = JSON.parse(res)

        const objectiveConfigId = ((res.objectiveList.filter(o => {
          return o.objectiveName === '课后巩固'
        }))[0].typeList.filter(o => {
          return o.name === '同步强化'
        }))[0].objectiveConfigId

        query = querystring.stringify(Object.assign({}, params, {
          type: 'INTELLIGENCE_EXAM',
          clazzs: '32090913_1298858',
          objectiveConfigId,
          _: Date.now(),
        }))

        fetch(`/teacher/new/homework/objective/content.api?${query}`, (res) => { // 获取知识点
          res = JSON.parse(res)
          // console.log(res)
          let knowledgePoints = []
          let packages = []

          for (let j = 0; j < res.content.length; j++) {
            if (res.content[j].type === 'package') {
              packages = res.content[j].packages
              break
            }
          }

          for (let i = 0; i < packages.length; i++) {
            if (packages[i].algoType === 'ENGLISH_LISTENING') {
              knowledgePoints = packages[i].knowledgePoints
              break
            }
          }

          fetchQuestion(bookId, unitId, knowledgePoints.map(kp => { // 根据知识点获取试题
            return kp.id
          }))
        })
      })
    }
  } else {
    fetchUnit()
  }
}

const fetchQuestion = (bookId, unitId, knowledgePoints) => { // 根据知识点获取试题
  fetch('/teacher/new/homework/intelligence/question.api', (res) => {
    res = JSON.parse(res)
    const fileName = `${bookName}-${unitName}`

    if (res.questions instanceof Array) {
      fetch('/exam/flash/load/question/byids.vpage', (question) => {
        fs.writeFileSync(`./assets/${fileName}.txt`, question, {
          encoding: 'utf-8',
          // flag: 'a'
        })
        console.log(`${fileName} fetch complete`)
        setTimeout(() => { // 每一分钟拉一个单元数据
          fetchKnowledgPpoint(bookId) // 继续捞下个单元的内容
        }, 30 * 1000)
      }, 'POST', {
        data: JSON.stringify({
          ids: res.questions.map(q => {
            return q.id
          })
        })
      })
    } else {
      console.log(`${fileName} has no questions.`)
      fetchKnowledgPpoint(bookId) // 继续捞下个单元的内容
    }
  }, 'POST', {
    bookId,
    unitId,
    sections: '',
    type: 'INTELLIGENCE_EXAM',
    subject: 'ENGLISH',
    clazzs: '32090913_1298858',
    difficulty: -1,
    algoType: 'ENGLISH_LISTENING',
    questionCount: 50,
    kpIds: knowledgePoints.join(','),
    contentTypeIds: '1031001,1031002'
  })
}

// fetchBook()
fetchUnit()