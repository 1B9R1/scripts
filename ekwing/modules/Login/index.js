// 导入库文件
const fs = require('fs')
const https = require('https')
const passport = require('./passport.js')

// 定义常量
const callbackTag = 'jQuery17209374718688604093_1514274149625'

// 定义方法
/**
 * 序列化GET的请求参数
 * @param {object} obj 
 */
const normalize = (obj) => {
  let str = ''

  for (let key in obj) {
    let value = obj[key] + ''
    if (value) {
      value = value.replace(/=/g, '%3D').replace(/\+/g, '%2B').replace(/\//g, '%2F')
    }
    str += '&' + key + '=' + value
  }

  // console.log(str)
  return str.substring(1)
}

/**
 * 解析JSONP的返回格式
 * @param {string} str 
 */
const unwrap = (str) => {
  return JSON.parse(str.replace(callbackTag + '(', '').replace(')', ''))
}

/**
 * 生成passport的url请求
 * @param {string} url 
 * @param {object} param 
 */
const getPassportUrl = (url, param) => {
  return `https://passport.ekwing.com/index/${url}?callback=${callbackTag}&${normalize(param)}&_=${+new Date()}`
}

module.exports = (callback) => {
  // 主程序入口
  https.get(getPassportUrl('getpublickey', {
    type: 'rsa'
  }), (res) => {
    // console.log('statusCode:', res.statusCode);
    // console.log('headers:', res.headers);
    let data = ''

    res.on('data', (d) => {
      data += d
    });

    res.on('end', () => {
      res = unwrap(data)
      passport.setPublicKey(res.data.public, res.data.key)

      const param = passport.getUserList({
        account: "%E7%BF%BC%E5%A4%A7%E5%A4%A7",
        passwd: "111111",
        schoolName: "%E5%8A%B1%E5%BF%97%E5%AD%A6%E6%A0%A1",
        vcode: ""
      })

      let data2 = ''
      let url = getPassportUrl('getschooluser', param)
      https.get(url, (res) => {
        res.on('data', (d) => {
          data2 += d
        })

        res.on('end', () => {
          let res2 = unwrap(data2)
          // console.log(res2)

          let param2 = passport.login({
            account: res2.data.user[0].uname,
            auto: 2,
            loginType: 4,
            passwd: "111111",
            realname: "%E7%BF%BC%E5%A4%A7%E5%A4%A7",
            schoolName: "%E5%8A%B1%E5%BF%97%E5%AD%A6%E6%A0%A1",
            vcode: ""
          })

          let url2 = getPassportUrl('login', param2)

          https.get(url2, (res) => {
            // console.log('headers:', res.headers['set-cookie'])
            const cookies = res.headers['set-cookie']
            const EKWUID = (cookies[0].split(';'))[0]
            // console.log(EKWUID)

            res.on('data', (d) => { /* 一定要写该事件，否则end事件不会触发 */ })

            res.on('end', () => {
              callback(EKWUID)
            })
          })
        })
      })
    })
  })
}