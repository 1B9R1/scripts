const md5 = require('./md5.js')
const RSA = require('./rsa.js')

function passportClass() {}

if ('undefined' == typeof (__LOGINDOMAIN)) {
	passportClass.__DOMAIN__ = "https://passport.ekwing.com";
} else {
	passportClass.__DOMAIN__ = __LOGINDOMAIN;
}
passportClass.requesting = false;
passportClass.keys = {
	publicKey: "",
	privateKey: ""
}
passportClass.schoolLogin = {
	account: ""
}

passportClass.verify = {
	status: false,
	token: ""
}

passportClass.getTimeStamp = function () {
	var times = new Date().getTime();
	times = times.toString();
	times = times.substr(0, 10);

	return times;
};

/**
 * 设置公钥和私钥
 * @param {string} publicKey 
 * @param {string} privateKey 
 */
passportClass.setPublicKey = (publicKey, privateKey) => {
	passportClass.keys.publicKey = publicKey
	passportClass.keys.privateKey = privateKey
}

passportClass.getVerifyImg = function () {
	if (!this.verify.status) {
		return "";
	}
	var requestUrl = this.__DOMAIN__ + "/index/getimage?token=" + this.verify.token + "&vtime=";

	var vtime = this.getTimeStamp() + Math.ceil(Math.random() * 10000);

	requestUrl += vtime;

	return requestUrl;
}

passportClass.showVerify = function (account, cb_showverify) {
	var requestUrl = this.__DOMAIN__ + "/index/logincheck";

	if (this.requesting) {
		return false;
	}
	this.requesting = true;

	$.ajax({
		type: 'POST',
		dataType: 'jsonp',
		async: false,
		url: requestUrl,
		data: {
			name: account,
			cilent_type: 'web'
		},
		jsonp: "callback",
		success: function (json) {
			passportClass.requesting = false;
			if (0 == json.errno) {
				if (json.data.bolTry > 0) {
					passportClass.verify = {
						status: true,
						token: json.data.token
					}
				} else {
					passportClass.verify = {
						status: false,
						token: ""
					}
				}

				cb_showverify();
			}
		}
	});
}

passportClass.login = function (params, callback) {
	// var requestUrl = this.__DOMAIN__ + "/index/login";
	passwd = md5(params.passwd);
	publicKey = RSA.getPublicKey(this.keys.publicKey);
	passwd = RSA.encrypt(passwd, publicKey);


	var postData = {
		uname: params.account,
		pw: passwd,
		client_type: "web",
		encrypt_key: this.keys.privateKey,
		encrypt_type:   "rsa",
		real_name: params.realname
	};

	if (4 == parseInt(params.loginType)) {
		postData.login_type = 4;
		postData.school_name = params.schoolName;
	}

	postData.mem_type = params.auto;

	if (this.verify.status) {
		postData.code = params.vcode;
		postData.code_token = this.verify.token;
	}

	return postData
}

passportClass.getUserList = function (params, callback) {
	// var requestUrl = this.__DOMAIN__ + "/index/getschooluser";
	passwd = md5(params.passwd);
	publicKey = RSA.getPublicKey(this.keys.publicKey);
	passwd = RSA.encrypt(passwd, publicKey);

	return postData = {
		uname: params.account,
		pw: passwd,
		encrypt_key: this.keys.privateKey,
		encrypt_type: "rsa",
		school_name: params.schoolName,
		login_type: 4,
		code: params.vcode,
		code_token: this.verify.token
	}
}

module.exports = passportClass