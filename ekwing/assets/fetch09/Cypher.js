const fs = require('fs')
const type = process.argv[2]
let content = fs.readFileSync(`./KP_${type}.txt`).toString()
let list = content.split('\n')
let cypher = 'MATCH (root:KP) WHERE root.name = "知识点"\n'
let count = 0
let tree = {}

const loop = (obj, keys, callback = () => {}) => {
  const key = keys.shift().replace('\r', '')
  if (!obj[key]) {
    obj[key] = {}
  }

  if (keys.length > 0) {
    loop(obj[key], keys, callback)
  } else {
    callback()
  }
}

const create = (obj, key = 'root') => {
  for (let kp in obj) {
    if (kp === '话题' || kp === '课标功能') {
      const _kp = kp === '话题' ? 'ht' : 'kb'
      cypher += `WITH kp1 MATCH (${_kp}:KP) WHERE ${_kp}.name = "${kp}"\n`
      cypher += `CREATE (${_kp})-[:BELONG]->(kp1)\n`
    } else {
      count += 1
      cypher += `CREATE (kp${count}:KP{name: "${kp}"})\n`
      cypher += `CREATE (kp${count})-[:BELONG]->(${key})\n`
  
      if (typeof obj[kp] === 'object') {
        create(obj[kp], `kp${count}`)
      }
    }
  }
}


list.forEach((one, idx) => {
  const keys = one.split(' > ')
  loop(tree, keys)
})
// console.log(tree)
create(tree)

fs.writeFileSync(`./KP_${type}_Cypher.txt`, cypher)
console.log('Finish')