/**
 * 获取翼课网知识图谱数据
 */

const fs = require('fs')
const login = require('../modules/login')
const request = require('../assists/request.js')
const {
  GET,
  POST
} = require('../assists/const.js')

let modelCount = 0
// module_type - 一级知识点
const kpList = [{
    id: 1,
    text: '语法'
  },
  {
    id: 2,
    text: '词汇'
  },
  {
    id: 4,
    text: '阅读'
  },
  {
    id: 8,
    text: '听力'
  },
  {
    id: 32,
    text: '写作'
  },
  {
    id: 16,
    text: '口语'
  },
]
//
const kpMap = {}
//
const models = {}

/**
 * 处理抓取到的题目列表
 * @param {array} models 
 * @param {string} kpId
 */
const dealWithModels = (modelList, kpId, prefix) => {
  for (let modelId in modelList) {
    const model = modelList[modelId]
    const {
      id,
      // ques_type,
      // model_type_name,
      // ques_list
    } = model

    if (!models[id]) {
      modelCount += 1
      models[id] = {
        kp: [kpId],
        aa: model
      }
    } else {
      fs.writeFileSync(`./assets/fetch09/duplicate.txt`, `${id}: ${prefix}\n`, {
        flag: 'a'
      })
      models[id].kp.push(kpId)
    }
  }
}

/**
 * 抓取知识点下面的题目
 * @param {string} kpId 子级知识点id
 * @param {string} moduleType 一级知识点id
 * @param {number} page 当前页
 * @param {number} total 总页数
 * @param {function} callback 回调
 */
const fetchKpModel = (kpId, moduleType, page, total, callback, prefix) => {
  page += 1

  if (page <= total) {
    const path = `/exam/review/getkpmodel?page=${page}&kp_id%5B%5D=${kpId}&module_type=${moduleType}&grade=0&model_type=0&level=0`
    request.fetch(path, GET, (res) => {
      const {
        model_list,
        pages
      } = JSON.parse(res).data
      console.log(`共${pages.match(/共\s(\d+)\s页/)[1]}页，当前第${page}页`)
      dealWithModels(model_list, kpId, prefix)
      fetchKpModel(kpId, moduleType, page, total, callback, prefix)
    })
  } else {
    callback()
  }
}

const createLoop = (moduleType, list, prefix, callback) => {
  let idx = -1
  let loop = () => {
    idx += 1

    if (idx < list.length) {
      const kp = list[idx]

      if (kp.has_sub) { // 如果有下级知识点
        const path = `/exam/review/getsubkp?module_type=${moduleType}&parent_kp_id=${kp.g}&student_uid=0&class_id=1634`
        // console.log(path)
        request.fetch(path, POST, (res) => {
          const {
            kp_list
          } = JSON.parse(res).data
          // console.log(`[dig into]${prefix}`)
          const fn = createLoop(moduleType, kp_list, prefix + ' > ' + kp.text, loop)
          fn()
        })
      } else {
        let content = `${prefix} > ${kp.text}(${kp.g})`
        console.log(content)
        // fs.writeFileSync(`./assets/fetch09/kp.txt`, content + '\n', {
        //   flag: 'a'
        // })
        // loop()
        request.fetch(`/exam/review/getkpmodel`, POST, (res) => {
          // console.log(res)
          const {
            model_list,
            pages
          } = JSON.parse(res).data

          if (pages) {
            const total = Number(pages.match(/共\s(\d+)\s页/)[1])
            console.log(`共${total}页，当前第1页`)
            dealWithModels(model_list, kp.g, content)
            if (total > 1) {
              fetchKpModel(kp.g, moduleType, 1, total, loop, content)
            } else {
              loop()
            }
          } else {
            loop()
          }
        }, {
          'kp_id[]': kp.g,
          module_type: moduleType,
          is_first: 1
        })
      }
    } else {
      callback()
    }
  }

  return loop
}

const fetchKpByModuleType = (callback) => {
  if (kpList.length > 0) {
    const kp = kpList.shift()

    request.fetch(`/exam/review/getsubkp?module_type=${kp.id}&parent_kp_id=0&student_uid=0&class_id=1634`, POST, (res) => {
      const {
        kp_list
      } = JSON.parse(res).data

      const loop = createLoop(kp.id, kp_list, kp.text, () => {
        fetchKpByModuleType(callback)
      })
      loop()
    })
  } else {
    callback()
  }
}

// 代码入口
login((id) => {
  const startTime = Date.now()
  request.init(id)

  fetchKpByModuleType(() => {
    fs.writeFileSync(`../assets/fetch09/model2.json`, JSON.stringify(models))
    console.log(`cost time: ${Date.now() - startTime}`)
    console.log(`model count: ${modelCount}`)
    console.log('finish')
  })
})

/**
 * https://www.ekwing.com/exam/review/getkpmodel - 请求题目
 * FormData
 * page: 2
 * kp_id[]:813
 * kp_id[]:832
 * module_type:1
 * is_first:1
 * is_auto:0
 * data.model_list.ques_list
 */