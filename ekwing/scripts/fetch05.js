/**
 * 抓取翼课网美文赏析、典范英语和朗文英语
 */

// 导入库文件
const fs = require('fs')
const login = require('../modules/login')
const request = require('../assists/request.js')
const {
  GET,
  POST,
  catalogs
} = require('../assists/const.js')

//
let catalog
let book
let articleCount = 0
let articles
let totalPage // 总页数
let page // 当前页

const fetchBookByCatalog = (callback) => {
  if (catalogs.length > 0) {
    catalog = catalogs.shift()
    fetchUnitsByBook(() => {
      fetchBookByCatalog(callback)
    })
  } else {
    callback()
  }
}

const fetchUnitsByBook = (callback, lastPage = 0) => {
  if (lastPage > 0 && lastPage <= totalPage) {
    page = lastPage + 1
    request.fetch(
      `/train/teacher/getunit?page=${page}&book_id=${book.book_id}&type=${catalog.type}&grade=${book.grade}`,
      GET,
      (data) => {
        data = JSON.parse(data).data
        articles = data.list
        fetchArticle(() => {
          fetchUnitsByBook(callback, page)
        })
      }
    )
  } else if (catalog.list.length > 0) {
    book = catalog.list.shift()
    request.fetch(
      `/train/teacher/getunit?page=1&book_id=${book.book_id}&type=${catalog.type}&grade=${book.grade}`,
      GET,
      (data) => {
        data = JSON.parse(data).data
        totalPage = data.total_page
        articles = data.list
        fetchArticle(() => {
          fetchUnitsByBook(callback, 1)
        })
      }
    )
  } else {
    callback()
  }
}

const fetchArticle = (callback) => {
  if (articles.length > 0) {
    const article = articles.shift()
    request.fetch(
      '/train/teacher/getunitinfo',
      POST,
      (res) => {
        const data = JSON.parse(res).data
        const title = `${book.name} - ${data.unit_info.title}`
        let content = ''
        let cnt = data.ext_content.content.replace(/&quot;/g, '\'').split('<br/>')
        let en = cnt
        let ch = []
        if (catalog.name === '典范英语') {

        } else {
          en = cnt.filter((sent, index) => {
            return index % 2 === 0
          })
          ch = cnt.filter((sent, index) => {
            return index % 2 === 1
          })
        }
        console.log(`Fetch ${title}`)
        content += `${title}\n`
        content += `音频地址：${data.ext_content.mp3}\n`
        content += `原文：\n`
        content += `${en.join('\n')}`
        content += `译文：\n`
        content += `${ch.join('\n')}`
        content += `\n\n`
        articleCount += 1
        fs.writeFileSync(`./assets/fetch05/${catalog.name}.txt`, content, {
          flag: 'a'
        })
        fetchArticle(callback)
      }, {
        book_id: article.book_id,
        unit_id: article.unit_id,
        type: article.type
      }
    )
  } else {
    callback()
  }
}

login((id) => {
  request.init(id)
  const startTime = +new Date()

  fetchBookByCatalog(() => {
    console.log(`Fetch ${articleCount} papers completed ......... !`)
    console.log(`Cost time: ${(+new Date()) - startTime}ms`)
  })
})