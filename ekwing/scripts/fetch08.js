/**
 * 抓取翼课网达标检测（听句子、听短对话、听长对话、听力判断、听对话选图片、朗读短文、交际问答）
 */

// 导入库文件
const fs = require('fs')
const login = require('../modules/login')
const request = require('../assists/request.js')
const {
  GET,
  POST,
  catalogs
} = require('../assists/const.js')

// 变量   
let fetchCount = 0
let levelIds = [{
  id: 2,
  name: '初中'
}, {
  id: 6,
  name: '高中'
}]
let levelId
let levelName
let pressList = []
let pressId
let pressName
let gradeList = []
let gradeId
let gradeName
let unitList = []
let unit
let unitId
let unitName
let listeningWorkList = []
let fileName

/**
 * 获取指定年级下的出版社
 */
const fetchPressByLevel = (callback) => {
  if (levelIds.length > 0) {
    const level = levelIds.shift()
    levelId = level.id
    levelName = level.name

    request.fetch(
      `/hw/getbooks?grade=${levelId}`,
      GET,
      (data) => {
        pressList = []
        data = JSON.parse(data).data.book_list
        for (let key in data) {
          pressList.push(data[key])
        }

        fetchGradeByPress(() => {
          fetchPressByLevel(callback)
        })
      }
    )
  } else {
    callback()
  }
}

/**
 * 获取指定出版社下的年级
 */
const fetchGradeByPress = (callback) => {
  if (pressList.length > 0) {
    const press = pressList.shift()
    pressId = press.id
    pressName = press.name

    request.fetch(
      `/hw/getbooklist?id=${pressId}`,
      GET,
      (data) => {
        gradeList = []
        data = JSON.parse(data).data
        for (let key in data) {
          gradeList.push(data[key])
        }

        fetchUnitByGrade(() => {
          fetchGradeByPress(callback)
        })
      }
    )
  } else {
    callback()
  }
}

/**
 * 获取指定年级下的单元列表
 * @param {function} callback 
 */
const fetchUnitByGrade = (callback) => {
  if (gradeList.length > 0) {
    const grade = gradeList.shift()
    gradeId = grade.id
    gradeName = grade.subname
    request.fetch(
      `/hw/getcatalog?id=${gradeId}&vid=${pressId}`,
      GET,
      (data) => {
        unitList = []
        data = JSON.parse(data).data
        for (let key in data) {
          unitList.push(data[key])
        }

        fetchAllWork(() => {
          fetchUnitByGrade(callback)
        })
      }
    )
  } else {
    callback()
  }
}

const fetchAllWork = (callback) => {
  if (unitList.length > 0) {
    unit = unitList.shift()
    unitId = unit.id
    unitName = unit.name.title1 + unit.name.topic1
    fileName = `${levelName}_${pressName}_${gradeName}_${unitName}`
    path = `/Hw/stepcheck?unit_id=${unitId}&book_vid=${pressId}&book_bid=${unit.book_id}`
    request.fetch(
      path,
      GET,
      (data) => {
        let content = data.match(/^.*$/mg).filter((sent) => {
          return sent.replace(/\t/g, '').replace(/\s/g, '') !== ''
        }).join('\n')
        
        const matches = data.replace(/\s+=\s+/g, '=').match(/<li\s(class="active"\s)?paper_type="\d+"\sdata-id="\d+"><a\shref="javascript:;"\stitle='([^']+)'>/g)
        // return console.log(matches)
        if (matches) {
          listeningWorkList = matches.map((m) => {
            work = {}
            const o = m.match(/<li\s(class="active"\s)?paper_type="(\d+)"\sdata-id="(\d+)"><a\shref="javascript:;"\stitle='([^']+)'>/)
            return {
              type: o[2],
              id: o[3],
              name: o[4]
            }
          })

          fetchWork(() => {
            fetchAllWork(callback)
          })
        } else {
          fetchAllWork(callback)
        }
      }
    )
  } else {
    callback()
  }
}

const fetchWork = (callback) => {
  if (listeningWorkList.length > 0) {
    const work = listeningWorkList.shift()
    const path = `/Hw/stepcheck?unit_id=${unitId}&book_vid=${pressId}&book_bid=${unit.book_id}&paper_id=${work.id}&paper_type=${work.type}`
    console.log(path)
    request.fetch(
      path,
      GET,
      (data) => {
        // return console.log(work.name)
        const _fileName = (fileName + `_${work.name}`).replace(/[:*?"]/g, '')
        // fileName = fileName.replace(/[:*?"]/g, '')
        let content = data.match(/^.*$/mg).filter((sent) => {
          return sent.replace(/\t/g, '').replace(/\s/g, '') !== ''
        }).map((sent) => {
          return sent.replace(/^\s+/, '')
        })
        content.splice(0, 63)
        content = content.join('\n')
        // console.log(data)
        fs.writeFileSync(`./assets/fetch08/${_fileName}.html`, content)
        fetchCount += 1
        fetchWork(callback)
      }
    )
  } else {
    callback()
  }
}

login((id) => {
  request.init(id)
  const startTime = +new Date()
  fetchPressByLevel(() => {
    console.log(`Fetch ${fetchCount} works completed ......... !`)
    console.log(`Cost time: ${(+new Date()) - startTime}ms`)
  })
})