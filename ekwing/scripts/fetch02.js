/**
 * 抓取翼课教材趣味配音（789年级）
 */

// 导入库文件
const fs = require('fs')
const login = require('../modules/login')
const request = require('../assists/request.js')
const {
  GET,
  POST,
  gradeIds,
  topicIds
} = require('../assists/const.js')

// 变量
let videoCount = 0
let topicIdsCopy
let videoListCopy
let currentGrade
let currentTopic

const fetchEnter = (callback) => {
  if (gradeIds.length > 0) {
    currentGrade = gradeIds.shift()
    topicIdsCopy = topicIds.concat([])

    fetchByGradeId(currentGrade.id, () => {
      fetchEnter(callback)
    })
  } else {
    callback()
  }
}

const fetchByGradeId = (gradeId, callback) => {
  if (topicIdsCopy.length > 0) {
    currentTopic = topicIdsCopy.shift()
    const path = `/train/teacher/getunit?page=1&unit_type=40&topic_id=${currentTopic.id}&grade=${gradeId}`

    request.fetch(
      path,
      GET,
      (data) => {
        try {
          list = JSON.parse(data).data.list
          videoListCopy = list.concat([])
          fetchVideo(() => {
            fetchByGradeId(gradeId, callback)
          })
        } catch (e) {
          console.log(data)
        }
      }
    )
  } else {
    callback()
  }
}

const fetchVideo = (callback) => {
  if (videoListCopy.length > 0) {
    const videoInfo = videoListCopy.shift()

    request.fetch(
      '/train/teacher/getunitinfo',
      POST,
      (data) => {
        data = JSON.parse(data).data
        let content = `${currentGrade.name} / ${currentTopic.name} / ${data.unit_info.title}`
        console.log(content)
        content += '\n'
        content += data.ext_content.video + '\n'
        content += data.ext_content.content.replace(/<br\/>/g, '\n').replace(/\n{2}/g, '\n') + '\n'
        fs.writeFileSync(`./assets/fetch02/videos.txt`, content, {
          flag: 'a'
        })
        videoCount += 1
        fetchVideo(callback)
      }, {
        book_id: videoInfo.book_id,
        unit_id: videoInfo.unit_id,
        type: videoInfo.type
      }
    )
  } else {
    callback()
  }
}

login((id) => {
  request.init(id)
  const startTime = +new Date()

  fetchEnter(() => {
    console.log(`Fetch ${videoCount} videos completed ......... !`)
    console.log(`Cost time: ${(+new Date()) - startTime}ms`)
  })
})