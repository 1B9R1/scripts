const fs = require('fs')
const _ = require('lodash')
const types = {}
const models = JSON.parse(fs.readFileSync('./assets/fetch09/model.txt').toString())
const dxxz = {}
let count = 0

for (let key in models) {
  count += 1
  const model = models[key]
  const type = model.model_type_name
  if (types[type]) {
    types[type] += 1
  } else {
    types[type] = 1
  }

  if (type === '单项选择') {
    const ques = model.ques_list[0]
    const answerIdx = ['A','B','C','D'].indexOf(ques.answer[0][0])
    const answers = ques.choose_list[answerIdx].text.split(';')
    let replaceIdx = 0
    const content = ques.title_text
      .replace(/<span\sclass="hole_min"\sph="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"\sdata-result=""><\/span>/g, '______')
      .replace(/([a-zA-Z])___/g, (str, $1) => {
        return `${$1} ___`
      })
      .replace(/_+/g, '______')
      .replace(/\s______/g, (str) => {
      const replaceStr = _.trim(answers[replaceIdx])
      replaceIdx += 1

      if (replaceStr === '/') {
        return ''
      } else {
        return ` (${replaceStr})`
      }
    })

    dxxz[key] = {
      kp: _.uniq(model.kp),
      content,
      choose_list: ques.choose_list.map((o) => {
        return o.text
      }),
      analyze: ques.analyze
    }
  }
}

console.log(`总题目数：${count}`)
console.log('=================')
Object.keys(types).forEach((key) => {
  console.log(`${key}: ${types[key]}`)
})

// fs.writeFileSync('./assets/fetch09/dxxz.json', JSON.stringify(dxxz))
fs.writeFileSync('./assets/fetch09/dxxz_simple.json', JSON.stringify(dxxz))