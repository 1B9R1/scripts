const fs =  require('fs')

const fileNames = fs.readdirSync('./assets/fetch08')
fileNames.shift()
fileNames.forEach((fileName) => {
  console.log(fileName)
  let content = fs.readFileSync(`./assets/fetch08/${fileName}`)
  let lines = content.toString()
                     .replace(/(该题型暂不支持手机答题)|(布置过)/g, '')
                     .replace(/&nbsp;/g, '')
                     .replace(/\n?<\/?[^>]+\/?>/g, '')
                     .replace(/\n([^\n]*)\n难度系数/g, (str, $1) => {
                       return `\n\n${$1}\n难度系数`
                     })
                     .replace(/([一二三四五六七八九十])、\n/g, (str, $1) => {
                       return `${$1}、`
                     })
                     .replace(/\)\n/g, ') ')
                    //  .replace(/答案：\s*\n/g, '答案：')
                     .replace(/\n，\n/g, ', ')
                     .split('\n')
  
  lines = lines.filter((line) => {
    return !/^-->/.test(line)
  }).map((line) => {
    return line.replace(/\s+/, ' ')
  })
  
  lines.splice(-6)
  
  // console.log(lines)
  fs.writeFileSync(`./assets/fetch08a/${fileName.split('.').shift()}.txt`, lines.join('\n'))
})
