/**
 * 获取翼课网的初中【趣味配音】资源
 */

// 导入库文件
const fs = require('fs')
const login = require('../modules/login')
const request = require('../assists/request.js')
const {
  GET,
  POST
} = require('../assists/const.js')
const txtNames = []

// 变量
let levelIds = [2] // [2, 4, 6] // [初中, 小学, 高中]
let level
let bookNames = []
let bookIds = []
let bookId
let bookName
let gradeNames = []
let gradeIds = []
let gradeId
let gradeName
let videoIds = []
let videoIdsCopy = []
let currentTxtName
let videoCount = 0

/**
 * 获取教材
 */
const fetchBooks = (callback) => {
  if (levelIds.length > 0) {
    level = levelIds.shift()
    request.fetch(
      `/hw/getbooks?grade=${level}`,
      GET,
      (data) => {
        data = JSON.parse(data).data.book_list
        
        for (let key in data) {
          const b = data[key]
          bookNames.push(b.name)
          bookIds.push(b.id)
        }

        fetchGrade(() => {
          fetchBooks(callback)
        })
      }
    )
  } else {
    callback()
  }
}

/**
 * 获取特定教材下的年级
 */
const fetchGrade = (callback) => {
  if (bookIds.length > 0) {
    bookId = bookIds.shift()
    bookName = bookNames.shift()
    request.fetch(
      `/hw/getbooklist?id=${bookId}`,
      GET,
      (data) => {
        data = JSON.parse(data).data

        for (let key in data) {
          const g = data[key]
          gradeNames.push(g.subname)
          gradeIds.push(g.id)
        }
  
        fetchCatalog(callback)
      }
    )
  } else {
    callback()
  }
}

/**
 * 获取单元列表
 * @param {function} callback 
 */
const fetchCatalog = (callback) => {
  if (gradeIds.length > 0) {
    gradeId = gradeIds.shift()
    gradeName = gradeNames.shift()
    request.fetch(
      `/hw/getcatalog?id=${gradeId}&vid=${bookId}`,
      GET,
      (data) => {
        data = JSON.parse(data).data
        let unitIds = []
    
        for (let key in data) {
          unitIds.push(key)
          const catalogName = data[key].name
          const txtName = `${bookName}_${gradeName}_${catalogName.title1}_${catalogName.topic1}`
          txtNames.push(txtName.replace(/[\/:*?"<>|]/g, '').replace(/\s+/g, '_'))
        }
    
        fetchVideos(unitIds, () => {
          // console.log('视频获取完毕')
          fetchCatalog(callback)
        })
      }
    )
  } else {
    callback()
  }
}

/**
 * 获取视频列表
 * @param {Array} unitIds 剩余的单元
 * @param {function} callback 
 */
const fetchVideos = (unitIds, callback) => {
  if (unitIds.length > 0) {
    const unitId = unitIds.shift()
    currentTxtName = txtNames.shift()
    request.fetch(
      '/hw/video',
      POST,
      (data) => {
        const books = data.match(/data-book-id="\d+"/g)
        const catalogs = data.match(/data-cat-id="\d+"/g)
        const videos = data.match(/data-id="\d+"/g)

        videoIds = []

        if (videos) {
          videos.forEach((video, index) => {
            videoIds.push([
              Number(books[index].match(/\d+/)[0]),
              Number(catalogs[index].match(/\d+/)[0]),
              Number(video.match(/\d+/)[0])
            ])
          })
        }

        videoIdsCopy = videoIds.concat([])

        fetchVideoById(() => {
          console.log(`#${unitId}视频获取完毕`)
          fetchVideos(unitIds, callback)
        })
      }, {
        unit_id: Number(unitId),
        cat_id: 0,
        grade_id: level,
        page: 0
      }
    )
  } else {
    callback()
  }
}

/**
 * 抓取视频信息
 * @param {function} callback 
 */
const fetchVideoById = (callback) => {
  if (videoIdsCopy.length > 0) {
    const videoId = videoIdsCopy.shift()
    request.fetch(
      '/hw/videodata',
      POST,
      (data) => {
        const videoName = data.match(/<span style="vertical-align: middle;">([^<]+)<\/span>/)[1]
        const difficulty = data.match(/<em class="cor-yellow">([^<]+)<\/em>/)[1]
        const src = data.match(/var\ssrc\s=\s"([^"]+)";/)[1]
        let content = `${videoName} - ${difficulty} - ${src}`
        console.log(content)
        content += '\n'
        content += data.replace(/<br>\s/g, '\n').match(/<div class="buf1 f12">[^<]+<\/div>/g).map((sent) => {
          return sent.replace(/[\r\n]/g, '').replace(/\s+/g, ' ').replace('<div class="buf1 f12">', '').replace('</div>', '')
        }).join('\n')
        content += '\n\n'
        fs.writeFileSync(`./assets/fetch01/${currentTxtName}.txt`, content, {
          flag: 'a'
        })
        videoCount += 1
        fetchVideoById(callback)
      }, {
        book_id: videoId[0],
        cat_id: videoId[1],
        unit_id: videoId[2]
      }
    )
  } else {
    callback()
  }
}

// 代码入口
login((id) => {
  request.init(id)
  const startTime = +new Date()
  fetchBooks(() => {
    console.log(`Fetch ${videoCount} videos completed ......... !`)
    console.log(`Cost time: ${(+new Date()) - startTime}ms`)
  })
})