/**
 * 抓取翼课网基础作业听力题
 */

// 导入库文件
const fs = require('fs')
const login = require('../modules/login')
const request = require('../assists/request.js')
const {
  GET,
  POST,
  catalogs
} = require('../assists/const.js')

// 变量
let fetchCount = 0
let levelIds = [{
  id: 2,
  name: '初中'
}, {
  id: 6,
  name: '高中'
}]
let levelId
let levelName
let pressList = []
let pressId
let pressName
let gradeList = []
let gradeId
let gradeName
let unitList = []
let unitId
let unitName
let listeningWorkList = []
let fileName

/**
 * 获取指定年级下的出版社
 */
const fetchPressByLevel = (callback) => {
  if (levelIds.length > 0) {
    const level = levelIds.shift()
    levelId = level.id
    levelName = level.name

    request.fetch(
      `/hw/getbooks?grade=${levelId}`,
      GET,
      (data) => {
        pressList = []
        data = JSON.parse(data).data.book_list
        for (let key in data) {
          pressList.push(data[key])
        }

        fetchGradeByPress(() => {
          fetchPressByLevel(callback)
        })
      }
    )
  } else {
    callback()
  }
}

/**
 * 获取指定出版社下的年级
 */
const fetchGradeByPress = (callback) => {
  if (pressList.length > 0) {
    const press = pressList.shift()
    pressId = press.id
    pressName = press.name

    request.fetch(
      `/hw/getbooklist?id=${pressId}`,
      GET,
      (data) => {
        gradeList = []
        data = JSON.parse(data).data
        for (let key in data) {
          gradeList.push(data[key])
        }

        fetchUnitByGrade(() => {
          fetchGradeByPress(callback)
        })
      }
    )
  } else {
    callback()
  }
}

/**
 * 获取指定年级下的单元列表
 * @param {function} callback 
 */
const fetchUnitByGrade = (callback) => {
  if (gradeList.length > 0) {
    const grade = gradeList.shift()
    gradeId = grade.id
    gradeName = grade.subname
    request.fetch(
      `/hw/getcatalog?id=${gradeId}&vid=${pressId}`,
      GET,
      (data) => {
        unitList = []
        data = JSON.parse(data).data
        for (let key in data) {
          unitList.push(data[key])
        }

        fetchAllWork(() => {
          fetchUnitByGrade(callback)
        })
      }
    )
  } else {
    callback()
  }
}

const fetchAllWork = (callback) => {
  if (unitList.length > 0) {
    const unit = unitList.shift()
    unitId = unit.id
    unitName = unit.name.title1 + unit.name.topic1
    fileName = `${levelName}_${pressName}_${gradeName}_${unitName}`.replace(/[:*?"]/g, '')

    let title = ''
    title += `试卷名称：${fileName}\n`
    title += `教材名称：${pressName}\n`
    title += `适用年级：${gradeName}\n`
    title += `所属单元：${unitName}\n\n`
    fs.writeFileSync(`../assets/fetch06/${fileName}.txt`, title)

    console.log(fileName)

    const path =`/hw/getsectcntlist?vid=${pressId}&bookId=${gradeId}&unitId=${unitId}&sectionId=0`
    request.fetch(
      path,
      GET,
      (data) => {
        data = JSON.parse(data).data
        try {
          listeningWorkList = data.BIZ_LISTENING.list
          fetchListeningWork(() => {
            fetchAllWork(callback)
          })
        } catch (e) {
          // console.log(`× ${fileName}`)
          fetchAllWork(callback)
        }
      }
    )
  } else {
    callback()
  }
}

const fetchListeningWork = (callback) => {
  if (listeningWorkList.length > 0) {
    const work = (listeningWorkList.shift()).list[0]
    const workName = work.name

    if (/听力理解/.test(workName) || /听音选词/.test(workName)) {
      const path = `/hw/preview?sid=${work.section_id}&biz=${work.biz}&path=${pressId}.${gradeId}.${unitId}.${work.section_id}&ids=${work.cnt_ids}`
      // console.log(path)
      request.fetch(
        path,
        GET,
        (data) => {
          let content = ''
          // return console.log(JSON.parse(data).data)
          data = JSON.parse(JSON.parse(data).data)

          content += `题型：${data.question.qus_title.text}\n`
          content += `难度：\n`
          content += `话题：\n`
          content += `音频地址：${data.audio}\n`
          content += `听力原文：\n`
          content += `${data.audioTxt}\n\n`
          if (workName === '听力理解') {
            data.question.qus_item.forEach((q, index) => {
              content += `${index + 1}. ${q.ask[0].content.replace(/^\s+/, '')}\n`
              q.items.forEach((item) => {
                content += `${item.key}. ${item.content[0].content}\n`
              })
              content += `答案：${q.answer[0]}\n`
              content += `分析：${q.analyz}\n\n`
            })
          } else {
            data.question.qus_item.forEach((q, index) => {
              if (q.ask && q.ask[0]) {
                content += `${index + 1}${q.ask[0].content}\n`
              } else {
                content += `${index + 1}\n`
              }

              q.items.forEach((item) => {
                content += `${item.key}. ${item.content[0].content}\n`
              })
              content += `答案：${q.answer[0]}\n`
              content += `分析：${q.analyz}\n\n`
            })
            // content += `分析：${data.base.analyze}\n`
          }
          // content += '\n'

          // console.log(`√ ${fileName}`)
          // console.log(data)
          fs.writeFileSync(`../assets/fetch06/${fileName}.txt`, content, {
            flag: 'a'
          })
          fetchCount += 1
          fetchListeningWork(callback)
        }
      )
    } else {
      // fetchListeningWork(callback)
      callback()
    }
  } else {
    callback()
  }
}

login((id) => {
  request.init(id)
  const startTime = +new Date()
  fetchPressByLevel(() => {
    console.log(`Fetch ${fetchCount} works completed ......... !`)
    console.log(`Cost time: ${(+new Date()) - startTime}ms`)
  })
})