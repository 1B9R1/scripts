/**
 * 抓取翼课网北京地区试卷
 */

// 导入库文件
const fs = require('fs')
const login = require('../modules/login')
const request = require('../assists/request.js')
const {
  GET,
  POST,
  Beijing
} = require('../assists/const.js')
const grade = ['七年级', '八年级', '九年级']
const cn = '一二三四五六七八九十'
const search_params = '{"exam_type":1,"grade_type":2,"grade":0,"paper_year":0,"paper_type":0,"province_id":"76","city_id":"1496","level":0,"model_type_publish":-1,"special_type":2,"publish_type":0,"city_name":"北京","province_name":"北京","ext":{"name_id":null}}'

//
let paperCount = 0
let papersInfoCopy
let gradeIndex = -1

const fetchGrade = (callback) => {
  gradeIndex += 1
  if (Beijing[gradeIndex]) {
    papersInfoCopy = Beijing[gradeIndex].concat([])
    fetchPaper(() => {
      fetchGrade(callback)
    })
  } else {
    callback()
  }
}

const fetchPaper = (callback) => {
  if (papersInfoCopy.length > 0) {
    const paperInfo = papersInfoCopy.shift()

    request.fetch(
      `/exam/special/ajaxgetmodellist?page=1`,
      POST,
      (data) => {
        let except = 0
        let cnIndex = -1
        let t1 = []
        let t2
        let t3
        let t4
        let content = ''
        data = JSON.parse(data).data
        console.log(`抓取试卷 #${data.base_info.paper_id} ${data.title}`)
        let modelList = []

        for (let key in data.model_list) {
          const model = data.model_list[key]
          const modelType = model.model_type
          if (modelType === '7') {
            t2 = model
          } else if (modelType === '1') {
            t4 = model
          } else if (modelType === '204') {
            t3 = model
          } else if (modelType === '2210' || modelType === '2211') {
            t1.push(model)
          } else {
            console.log(`【${data.title}】含有范围外试题：${modelType}`)
            except += 1
          }
        }

        if (except === 0) {
          content += `试卷名称：${data.title}\n`
          content += `教材名称：\n`
          content += `适用年级：${grade[gradeIndex]}\n`
          content += `所属单元：\n\n`
  
          if  (t1.length > 0) {
            cnIndex += 1
            content += `${cn[cnIndex]}、听后选择\n`
            t1.forEach((q) => {
              content += `难度：${q._item_info.level}\n`
              content += `话题：${q._item_info.show_topic}\n`
              content += `音频地址：${q.title_audio}\n`
              content += `听力原文：\n`
              content += `${q.listen_ori}\n\n`
              q.ques_list.forEach((o, i) => {
                content += `${i+1}. ${o.title_text}\n`
                o.choose_list.forEach((c) => {
                  content += `${c.name}. ${c.text}\n`
                })
                content += `答案：${o.answer[0][0]}\n`
                content += `分析：${o.analyze}\n`
                content += `\n`
              })
            })
            content += `\n`
          }
  
          if (t2) {
            cnIndex += 1
            content += `${cn[cnIndex]}、听后回答\n`
            content += `难度：${t2._item_info.level}\n`
            content += `话题：\n`
            t2.ques_list.forEach((q, index) => {
              const t = t2.title[index]
              content += `音频地址：${t.audio}\n`
              content += `听力原文：\n`
              content += `${t.text}\n\n`
              content += `问题：${q.title_text}\n`
              content += `答案：\n`
              q.answer.forEach((a) => {
                content += `${a[0]}\n`
              })
              content += `关键字：\n`
              content += `\n`
            })
            content += `\n`
          }
  
          if (t3) {
            cnIndex += 1
            content += `${cn[cnIndex]}、听后记录并转述\n`
            content += `难度：${t3._item_info.level}\n`
            content += `话题：\n`
            content += `图片地址：${t3.title_pic}\n`
            content += `音频地址：${t3.title_audio}\n`
            content += `听力原文：\n`
            content += `${t3.title_text}\n\n`
            content += `第一节答案：\n`
            t3.ques_list.forEach((q, index) => {
              content += `${index}. ${q.answer.join(' / ')}\n`
            })
            content += `\n第二节答案：\n`
            t3.answer.forEach((a) => {
              content += `${a[0]}\n`
            })
            content += `\n`
          }
  
          if (t4) {
            cnIndex += 1
            content += `${cn[cnIndex]}、朗读短文\n`
            content += `难度：${t4._item_info.level}\n`
            content += `${t4.real_text}`
          }
  
          fs.writeFileSync(`../assets/fetch04/${data.title}.txt`, content)
          paperCount += 1
        }
        
        fetchPaper(callback)
      }, {
        search_params,
        paper_id: paperInfo.id,
        page: 1,
        page_from: 'special'
      }
    )
  } else {
    callback()
  }
}

login((id) => {
  request.init(id)
  const startTime = +new Date()

  fetchGrade(() => {
    console.log(`Fetch ${paperCount} papers completed ......... !`)
    console.log(`Cost time: ${(+new Date()) - startTime}ms`)
  })
})