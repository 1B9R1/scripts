module.exports = {
  GET: 'GET',
  POST: 'POST',
  gradeIds: [
    {
      id: 7,
      name: '七年级'
    }, {
      id: 8,
      name: '八年级'
    }, {
      id: 9,
      name: '九年级'
    }
  ],
  topicIds: [
    {
      id: '5478',
      name: '个人情况(Personal background)'
    }, {
      id: '5484',
      name: '家庭、朋友与周围的人(Family, friends and people around)'
    }, {
      id: '5488',
      name: '居住环境(Living environment)'
    }, {
      id: '5493',
      name: '日常活动(Daily routines)'
    }, {
      id: '5497',
      name: '学校(School)'
    }, {
      id: '5502',
      name: '个人兴趣(Personal interests)'
    }, {
      id: '5507',
      name: '情感与情绪(Feelings and moods)'
    }, {
      id: '5510',
      name: '人际交往(Interpersonal communication)'
    }, {
      id: '5513',
      name: '计划与安排(Plans and arrangements)'
    }, {
      id: '5516',
      name: '节假日活动(Festivals, holidays and celebrations)'
    }, {
      id: '5519',
      name: '购物(Shopping)'
    }, {
      id: '5525',
      name: '饮食(Food and drinks)'
    }, {
      id: '5530',
      name: '卫生与健康(Hygiene and health)'
    }, {
      id: '5537',
      name: '安全与救护(Safety and first aid)'
    }, {
      id: '5542',
      name: '天气(Weather)'
    }, {
      id: '5546',
      name: '文娱与体育(Recreation and sports)'
    }, {
      id: '5550',
      name: '旅游与交通(Travel and transport)'
    }, {
      id: '5555',
      name: '通讯(Communications)'
    }, {
      id: '5559',
      name: '语言学习(Language learning)'
    }, {
      id: '5564',
      name: '自然(Nature)'
    }, {
      id: '5570',
      name: '世界与环境(The world and the environment)'
    }, {
      id: '5574',
      name: '科普知识与现代技术(Popular science and modern technology)'
    }, {
      id: '5579',
      name: '历史与社会(History and society)'
    }, {
      id: '5582',
      name: '故事与诗歌(Stories and poems)'
    }, {
      id: '5630',
      name: '热点话题(Hot issues)'
    }
  ],
  Beijing: [
    [{
        id: "122214",
        title: "大兴区2017-2018学年初一期末听说考试题"
      },
      {
        id: "110431",
        title: "北师大七上Unit1听说试题1"
      },
      {
        id: "110432",
        title: "北师大七上Unit1听说试题2"
      },
      {
        id: "122754",
        title: "北师大七上Unit2听说试题1"
      },
      {
        id: "122759",
        title: "北师大七上Unit2听说试题2"
      },
      {
        id: "122753",
        title: "北师大七上Unit3听说试题1"
      },
      {
        id: "122755",
        title: "北师大七上Unit3听说试题2"
      },
      {
        id: "110429",
        title: "2017延庆七上期中听说试题"
      },
      {
        id: "122757",
        title: "北师大七上Unit4听说试题1"
      },
      {
        id: "122760",
        title: "北师大七上Unit4听说试题2"
      },
      {
        id: "120184",
        title: "大兴区2017-2018学年初一联考11月试题"
      },
      {
        id: "109320",
        title: "北师大七上Get Ready A&B&C听说题1"
      },
      {
        id: "109321",
        title: "北师大七上Get Ready A&B&C听说题2"
      },
      {
        id: "109886",
        title: "北师大七上Get Ready D&E听说题1"
      },
      {
        id: "109884",
        title: "北师大七上Get Ready D&E听说题2"
      },
      {
        id: "102083",
        title: "北京市初一口语练习题Module2"
      },
      {
        id: "102084",
        title: "北京市初一口语练习题Module3"
      },
      {
        id: "102085",
        title: "北京市初一口语练习题Module4"
      },
      {
        id: "102086",
        title: "北京市初一口语练习题Module5"
      },
      {
        id: "102087",
        title: "北京市初一口语练习题Module6"
      },
      {
        id: "102088",
        title: "北京市初一口语练习题Module7"
      },
      {
        id: "102089",
        title: "北京市初一口语练习题Module8"
      },
      {
        id: "102090",
        title: "北京市初一口语练习题Module9"
      },
      {
        id: "102091",
        title: "北京市初一口语练习题Module10"
      },
      {
        id: "102092",
        title: "北京市初一口语练习题Module11"
      },
      {
        id: "102093",
        title: "北京市初一口语练习题Module12"
      },
      {
        id: "103733",
        title: "2016朝阳区七上期末口语试题"
      },
      {
        id: "103409",
        title: "北京外研七下Module1口语题"
      },
      {
        id: "103410",
        title: "北京外研七下Module2口语题"
      },
      {
        id: "103411",
        title: "北京外研七下Module3口语题"
      },
      {
        id: "103412",
        title: "北京外研七下Module4口语题"
      },
      {
        id: "103413",
        title: "北京外研七下Module5口语题"
      },
      {
        id: "103414",
        title: "北京外研七下Module6口语题"
      },
      {
        id: "103415",
        title: "北京外研七下Module7口语题"
      },
      {
        id: "103416",
        title: "北京外研七下Module8口语题"
      },
      {
        id: "103417",
        title: "北京外研七下Module9口语题"
      },
      {
        id: "103418",
        title: "北京外研七下Module10口语题"
      },
      {
        id: "103419",
        title: "北京外研七下Module11口语题"
      },
      {
        id: "103420",
        title: "北京外研七下Module12口语题"
      },
      {
        id: "109769",
        title: "2017年北京大兴七上听说试题"
      }
    ],
    [{
        id: "122215",
        name: "大兴区2017-2018学年初二期末听说考试题"
      },
      {
        id: "109888",
        name: "北师大八上Unit1听说题"
      },
      {
        id: "109891",
        name: "北师大八上Unit2听说题"
      },
      {
        id: "109894",
        name: "北师大八上Unit3听说题"
      },
      {
        id: "109889",
        name: "北师大八上Unit4听说题"
      },
      {
        id: "109890",
        name: "北师大八上Unit5听说题"
      },
      {
        id: "109892",
        name: "北师大八上Unit6听说题"
      },
      {
        id: "103311",
        name: "北师大版八上Unit1Lesson3口语试题"
      },
      {
        id: "103314",
        name: "北师大版八上Unit2Lesson5口语试题"
      },
      {
        id: "103315",
        name: "北师大版八上Unit2Lesson6口语试题"
      },
      {
        id: "103316",
        name: "北师大版八上Unit2综合口语试题"
      },
      {
        id: "103318",
        name: "北师大版八上Unit3Lesson8口语试题"
      },
      {
        id: "103319",
        name: "北师大版八上Unit3Lesson9口语试题"
      },
      {
        id: "103320",
        name: "北师大版八上Unit3综合口语试题"
      },
      {
        id: "103734",
        name: "2016大兴区八上期末口语试题"
      },
      {
        id: "103732",
        name: "2016朝阳区八上期末口语试题"
      },
      {
        id: "102295",
        name: "人教八上期中听力复习题"
      },
      {
        id: "102336",
        name: "外研八上期末听力复习题"
      },
      {
        id: "102319",
        name: "人教八上期末听力复习题"
      },
      {
        id: "102227",
        name: "外研八上期中听力复习题"
      },
      {
        id: "102223",
        name: "人教八上Unit2听力试题"
      },
      {
        id: "102228",
        name: "人教八上Unit3听力试题"
      },
      {
        id: "102233",
        name: "人教八上Unit4听力试题"
      },
      {
        id: "102239",
        name: "人教八上Unit5听力试题"
      },
      {
        id: "102250",
        name: "人教八上Unit6听力试题"
      },
      {
        id: "102255",
        name: "人教八上Unit7听力试题"
      },
      {
        id: "102262",
        name: "人教八上Unit8听力试题"
      },
      {
        id: "102274",
        name: "人教八上Unit9听力试题"
      },
      {
        id: "102286",
        name: "人教八上Unit10听力试题"
      },
      {
        id: "102205",
        name: "外研八上Module3听力试题"
      },
      {
        id: "102210",
        name: "外研八上Module4听力试题"
      },
      {
        id: "102213",
        name: "外研八上Module5听力试题"
      },
      {
        id: "102222",
        name: "外研八上Module6听力试题"
      },
      {
        id: "102235",
        name: "外研八上Module7听力试题"
      },
      {
        id: "102238",
        name: "外研八上Module8听力试题"
      },
      {
        id: "102245",
        name: "外研八上Module9听力试题"
      },
      {
        id: "102306",
        name: "外研八上Module10听力试题"
      },
      {
        id: "102309",
        name: "外研八上Module11听力试题"
      },
      {
        id: "102326",
        name: "外研八上Module12听力试题"
      },
      {
        id: "108450",
        name: "北师大八下Unit1听说题"
      },
      {
        id: "108451",
        name: "北师大八下Unit2听说题"
      },
      {
        id: "108452",
        name: "北师大八下Unit3听说题"
      },
      {
        id: "108453",
        name: "北师大八下Unit4听说题"
      },
      {
        id: "108454",
        name: "北师大八下Unit5听说题"
      },
      {
        id: "108455",
        name: "北师大八下Unit6听说题"
      },
      {
        id: "108389",
        name: "北京市八下期中听说测试题1"
      },
      {
        id: "108390",
        name: "北京市八下期中听说测试题2"
      },
      {
        id: "108907",
        name: "北京朝阳八下期末听说考试题"
      },
      {
        id: "108449",
        name: "外研八下Module1听说题"
      },
      {
        id: "108456",
        name: "外研八下Module2听说题"
      },
      {
        id: "108457",
        name: "外研八下Module3听说题"
      },
      {
        id: "108458",
        name: "外研八下Module4听说题"
      },
      {
        id: "108459",
        name: "外研八下Module5听说题"
      },
      {
        id: "108460",
        name: "外研八下Module6听说题"
      },
      {
        id: "108461",
        name: "外研八下Module7听说题"
      },
      {
        id: "108462",
        name: "外研八下Module8听说题"
      },
      {
        id: "103721",
        name: "外研八下Module9口语题"
      },
      {
        id: "103722",
        name: "外研八下Module10口语题"
      },
      {
        id: "109800",
        name: "2017年北京大兴八上听说试题"
      },
      {
        id: "110430",
        name: "大兴区2017-2018学年初二联考11月试题"
      }
    ],
    [{
        id: "110423",
        name: "外研九上Module1听说题"
      },
      {
        id: "110427",
        name: "外研九上Module2听说题"
      },
      {
        id: "110428",
        name: "外研九上Module3听说题"
      },
      {
        id: "110416",
        name: "外研九上Module4听说题"
      },
      {
        id: "110418",
        name: "外研九上Module5听说题"
      },
      {
        id: "110419",
        name: "外研九上Module6听说题"
      },
      {
        id: "110424",
        name: "外研九上Module7听说题"
      },
      {
        id: "110425",
        name: "外研九上Module8听说题"
      },
      {
        id: "110422",
        name: "外研九上Module9听说题"
      },
      {
        id: "110420",
        name: "外研九上Module10听说题"
      },
      {
        id: "110421",
        name: "外研九上Module11听说题"
      },
      {
        id: "110417",
        name: "外研九上Module12听说题"
      },
      {
        id: "110342",
        name: "《学程》配套练习Unit1"
      },
      {
        id: "110340",
        name: "《学程》配套练习Unit2"
      },
      {
        id: "110341",
        name: "《学程》配套练习Unit3"
      },
      {
        id: "110345",
        name: "《学程》配套练习Unit4"
      },
      {
        id: "122292",
        name: "《学程》配套练习Unit5"
      },
      {
        id: "122311",
        name: "《学程》配套练习Unit6"
      },
      {
        id: "110415",
        name: "《学程》配套练习Unit8"
      },
      {
        id: "109279",
        name: "北京中考听说考试模拟题3"
      },
      {
        id: "109284",
        name: "北京中考听说考试模拟题7"
      },
      {
        id: "109286",
        name: "北京中考听说考试模拟题9"
      },
      {
        id: "109271",
        name: "北京中考听说考试模拟题11"
      },
      {
        id: "109272",
        name: "北京中考听说考试模拟题12"
      },
      {
        id: "109273",
        name: "北京中考听说考试模拟题13"
      },
      {
        id: "109274",
        name: "北京中考听说考试模拟题14"
      },
      {
        id: "109275",
        name: "北京中考听说考试模拟题15"
      },
      {
        id: "102327",
        name: "人教九全期末听力复习题"
      },
      {
        id: "102267",
        name: "人教九全Unit1听力试题"
      },
      {
        id: "102269",
        name: "人教九全Unit2听力试题"
      },
      {
        id: "102284",
        name: "人教九全Unit3听力试题"
      },
      {
        id: "102287",
        name: "人教九全Unit4听力试题"
      },
      {
        id: "102290",
        name: "人教九全Unit5听力试题"
      },
      {
        id: "102291",
        name: "人教九全Unit6听力试题"
      },
      {
        id: "102292",
        name: "人教九全Unit7听力试题"
      },
      {
        id: "102296",
        name: "人教九全Unit8听力试题"
      },
      {
        id: "102298",
        name: "人教九全Unit9听力试题"
      },
      {
        id: "102303",
        name: "人教九全Unit10听力试题"
      },
      {
        id: "102308",
        name: "人教九全Unit11听力试题"
      },
      {
        id: "102311",
        name: "人教九全Unit12听力试题"
      },
      {
        id: "102316",
        name: "人教九全Unit13听力试题"
      },
      {
        id: "102216",
        name: "外研九上Module3听力试题"
      },
      {
        id: "102230",
        name: "外研九上Module4听力试题"
      },
      {
        id: "102248",
        name: "外研九上Module5听力试题"
      },
      {
        id: "102261",
        name: "外研九上Module6听力试题"
      },
      {
        id: "102299",
        name: "外研九上Module7听力试题"
      },
      {
        id: "102305",
        name: "外研九上Module8听力试题"
      },
      {
        id: "102337",
        name: "外研九上Module9听力试题"
      },
      {
        id: "102359",
        name: "外研九上Module10听力试题"
      },
      {
        id: "102382",
        name: "外研九上Module11听力试题"
      },
      {
        id: "102399",
        name: "外研九上Module12听力试题"
      },
      {
        id: "102229",
        name: "外研九下期中听力复习题"
      },
      {
        id: "102184",
        name: "外研九下Module1听力试题"
      },
      {
        id: "102194",
        name: "外研九下Module2听力试题"
      },
      {
        id: "102209",
        name: "外研九下Module3听力试题"
      },
      {
        id: "102219",
        name: "外研九下Module4听力试题"
      },
      {
        id: "102224",
        name: "外研九下Module5听力试题"
      },
      {
        id: "102249",
        name: "外研九下Module6听力试题"
      },
      {
        id: "102260",
        name: "外研九下Module7听力试题"
      },
      {
        id: "102265",
        name: "外研九下Module8听力试题"
      }
    ]
  ],
  catalogs: [
    {
      name: '美文欣赏',
      type: 1007,
      list: [{
        book_id: 3219,
        grade: 7,
        name: '七年级上'
      }, {
        book_id: 3220,
        grade: 7,
        name: '七年级下'
      }, {
        book_id: 3221,
        grade: 8,
        name: '八年级上'
      }, {
        book_id: 3222,
        grade: 8,
        name: '八年级下'
      }, {
        book_id: 3223,
        grade: 9,
        name: '九年级全'
      }]
    }, {
      name: '典范英语',
      type: 1503,
      list: [{
        book_id: 3202,
        grade: 7,
        name: '典范英语7 - 七年级'
      }, {
        book_id: 3203,
        grade: 8,
        name: '典范英语8 - 八九年级'
      }, {
        book_id: 3204,
        grade: 9,
        name: '典范英语9 - 九年级'
      }, {
        book_id: 3238,
        grade: 7,
        name: '典范英语4a(new) - 六七八年级'
      }, {
        book_id: 4612,
        grade: 7,
        name: '典范英语4b(new) - 六七八年级'
      }, {
        book_id: 4713,
        grade: 7,
        name: '典范英语5a(new) - 六七八年级'
      }, {
        book_id: 4714,
        grade: 7,
        name: '典范英语5b(new) - 六七八年级'
      }, {
        book_id: 5577,
        grade: 6,
        name: '典范英语6a(new) - 六年级'
      }, {
        book_id: 5578,
        grade: 6,
        name: '典范英语6b(new) - 六年级'
      }]
    }, {
      name: '朗文英语',
      type: 1539,
      list: [{
        book_id: 4725,
        grade: 6,
        name: '六年级 - 朗文英文6A'
      }, {
        book_id: 4726,
        grade: 6,
        name: '六年级 - 朗文英文6B'
      }]
    }
  ]
}