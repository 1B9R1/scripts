const https = require('https')
const querystring = require('querystring')

let EKWUID

module.exports = {
  init(id) {
    EKWUID = id
  },
  fetch(path, method, callback, params = {}) {
    const req = https.request({
      hostname: 'www.ekwing.com',
      path,
      method,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        Cookie: EKWUID,
        'X-Requested-With': 'XMLHttpRequest'
      }
    }, (res) => {
      let data = ''

      res.on('data', (d) => {
        data += d
      })

      res.on('end', () => {
        callback(data)
      })
    })

    req.on('error', (e) => {
      console.error(`request ${path} has a problem: ${e.message}`)
    });

    req.write(querystring.stringify(params))
    req.end()
  }
}